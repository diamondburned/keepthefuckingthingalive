package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	a2s "github.com/rumblefrog/go-a2s"
)

var (
	config = cfg()
	cli    = &http.Client{}
	oa     = &OAuth{}
)

// Config json
type Config struct {
	Puffer struct {
		PanelAddress string
		ClientID     string
		ClientSecret string
		Username     string
		Password     string
	}
	// ["gameserver address"]id
	Servers map[string]string
}

type OAuth struct {
	AccessToken string `json:"access_token"`
	Expires     int64  `json:"expires"`
	TokenType   string `json:"token_type"`
	Scope       string `json:"scope"`
}

func cfg() Config {
	file, err := os.OpenFile("./config.json", os.O_CREATE|os.O_RDWR|os.O_SYNC, os.ModePerm)
	if err != nil {
		panic(err)
	}

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	var cfg Config
	if err := json.Unmarshal(bytes, &cfg); err != nil {
		log.Println("Invalid config.")
		j, _ := json.MarshalIndent(&Config{}, "", "\t")
		log.Println(string(j))
		panic(err)
	}

	return cfg
}

func main() {
	duration := time.Duration(5) * time.Second
	for addr, id := range config.Servers {
		q, _ := a2s.NewClient(addr, a2s.TimeoutOption(duration))
		_, err := q.QueryInfo()
		if err == nil {
			println("Server", addr, id, "alive")
			continue
		} else {
			if oa == nil || oa.AccessToken == "" {
				oa = NewToken()
			}

			println("Server", addr, id, "downed, killing")
			if err := oa.act(id, "kill"); err != nil {
				println(err.Error())
				continue
			}

			time.Sleep(time.Duration(1) * time.Second)

			println("Server", addr, id, "killed, starting")
			if err := oa.act(id, "start"); err != nil {
				println(err.Error())
				continue
			}

			println("Finished")
		}
	}

}

func NewToken() *OAuth {
	form := &url.Values{}

	if config.Puffer.Username != "" && config.Puffer.Password != "" {
		form.Add("grant_type", "password")
		form.Add("username", config.Puffer.Username)
		form.Add("password", config.Puffer.Password)
	} else if config.Puffer.ClientID != "" && config.Puffer.ClientSecret != "" {
		form.Add("grant_type", "client_credentials")
		form.Add("client_id", config.Puffer.ClientID)
		form.Add("client_secret", config.Puffer.ClientSecret)
	} else {
		log.Println("Invalid config")
		os.Exit(1)
	}

	oaReq, err := http.NewRequest(
		"POST",
		config.Puffer.PanelAddress+"/oauth2/token/request?"+form.Encode(),
		nil,
	)

	if err != nil {
		panic(err)
	}

	oaResp, err := cli.Do(oaReq)
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(oaResp.Body)
	if err != nil {
		panic(err)
	}

	oaResp.Body.Close()

	if oaResp.StatusCode != 200 {
		panic(string(body))
	}

	var oa OAuth
	if err := json.Unmarshal(body, &oa); err != nil {
		panic(err)
	}

	return &oa
}

func (oa *OAuth) act(id string, action string) error {
	request, err := http.NewRequest(
		"GET",
		config.Puffer.PanelAddress+"/daemon/server/"+id+"/"+action,
		nil,
	)

	if err != nil {
		log.Println(err.Error())
		return err
	}

	request.Header.Set("Authorization", "Bearer "+oa.AccessToken)

	response, err := cli.Do(request)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	defer response.Body.Close()

	if response.StatusCode != 200 {
		body, _ := ioutil.ReadAll(response.Body)
		log.Println("Resp not 200:", response.Status, body)
		return errors.New(string(body))
	}

	return nil
}
